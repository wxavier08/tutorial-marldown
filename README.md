# Tutorial-MarkDown

#### Fala DEV, se você quer aprender os principais comandos para começar a usar o MarkDown, você está no lugar certo!!

##### Mas o que seria esse tal de MarkDown??? 
Basicament o **MarkDown** seria uma forma de escrever (sintaxe) para a formatação de textos para Web, muito utilizada em plataformas como o GitHub. Com certeza, se você já viu
algum repositório no GitHub, viu uma bela descrição qdo projeto que provavelmente foi escrita utilizando o **MarkDown** (todo bom projeto tem um bom README, lembre-se disso).

## Títulos 

Para escrever títulos basta utilizar '#' antes do texto que deseja utilizar. A quantidade de '#' que é utlizada determina o tipo de título, que são equivalentes as tags "h"
utiliazadas no HTML.

Veja no exemplo abaixo: 

```
#titulo 1 - referente ao <h1>
## titulo 2 - referentere a <h2>
### titulo 3 - referentere a <h3>
#### titulo 4 - referentere a <h4>
##### titulo 5 - referentere a <h5>
###### titulo 6 - referentere a <h6>

```

## Estilo de texto


No MarkDown também temos a opção de estilizar os textos com negrito e itálico. 

### Negrito

Para usar seus textos em **negrito** é preciso escrever o texto entre dois ```**``` ou ```__```. Veja o exemplo: 

```
Dev é a **melhor** profissão do mundo!!!

Dev é a __melhor__ profissão do mundo!!!

```

Ambos retornarão como resultado: "Dev é a **melhor** profissão do mundo!!!"


### Itálico

Para usar seus texto em *Itálico* é preciso escrever textos os textos entre um ```*``` ou ```_```. Veja o exemplo:

```
Dev é a *melhor* profissão do mundo!!!

Dev é a _melhor_ profissão do mundo!!!

```

Ambos retronarão como resultado "Dev é a *melhor* profissão do mundo!!!".

Ainda há a opção de utilizar as duas estilizações juntas. Há varias formas de fazer isso. Veja no exemplo:

```
Javascript é **_TOP_**
Javascript é *___TOP__*
Javascript é _**TOP**_
Javascript é __*TOP*__
```

Todas retornarão "Javascript é **_TOP_**" como resultado.
 
## Trechos de código 

Se você quiser enriquecer ainda mais seu README mostrando trechos de seu código basta utilizar 3 crases (" ` ") ou til ("~"). Você também pode inserir qual a liguagem
utilizada nesse trecho de código. Veja no exemplo o resultado com o trecho de código em Javascript:

```javascript
console.log('Hello World!!)
```

## Links

Existem duas formas de utilzar links com Markdown, o que chamamos de texto-âncora e o link-direto

### Texto-âncora
O texto-âncora é utilizado quando om onjeitov é que uma palavra especifíca, ao ser clicada, direciona para o link desejado. Para isso utilizamos os caracteres ``` []() ```, colocando
entre os colchetes o texto desejado e entre os parênteses o endereço de destino, como no exemplo:

```
Acesse o [youtube](https://www.youtube.com/)
```
O resultado retornado é: "Acesse o [youtube](https://www.youtube.com/)"

### Link-direto
O link direto é utilizado para mostrar para mostrar o endereço clicável. Para isso, escreva o endereço entre os caracteres ``` <> ```. veja no exemplo:

```
Acesse o <https://www.youtube.com/>
```

O resultado retornado é: "Acesse o <https://www.youtube.com/>"

## Listas

Assim como no HTML, utilizando o MarkDown há a possibilidade de usar listas ordenadas e não ordenadas. 

### Listas ordenadas

Para utilizar listas ordenadas, utilize o número de elemento seguido do ponto. Veja o exemplo:

```
Listas de linguagens de programação:

1. Javascript
2. PHP
3. Python

```
### Listas não-ordenadas

Para utilizar lista ordenadas, basta utilizar um asterisco antes dos elementos da lista.

```
Listas de linguagens de programação:

* Javascript
* PHP
* Python
```


## CheckBox
Para criar CheckBox utilizando MarkDown, utilize a seguinte sintaxe:

```
Tarefas diárias: 

- [x] Acordar
- [ ] Comer
- [ ] Codar

```

O resultado obtido será a seguinte lista: 

Tarefas diárias:

- [x] Acordar
- [ ] Comer
- [ ] Codar




## Tabelas

Para utilizar tabelas basta digitar os títulos das colunas separados por um ```|```, depis utilize o o hífen ```-``` para indicar que esse que acima dele estão os títulos
e abaixo são os dados da tabela. Adicione quantos dados desejar, sempre utilizando ```|``` para separar as colunas. Veja no exemplo:

```
Frutas| Preço
----- | -----
 Maçã | R$ 1
Pêra  | R$ 2
Uva   | R$ 3
Morango | R$ 4
```
O resultado será a seguinte tabela: 

Frutas| Preço
----- | -----
 Maçã | R$ 1
Pêra  | R$ 2
Uva   | R$ 3
Morango | R$ 4





